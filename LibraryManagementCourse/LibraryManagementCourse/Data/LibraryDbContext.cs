﻿using LibraryManagementCourse.Data.Model;
using Microsoft.EntityFrameworkCore;

namespace LibraryManagementCourse.Data
{
    public class LibraryDbContext : DbContext
    {
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Book> Books { get; set; }
        public DbSet<Author> Authors { get; set; }

        public LibraryDbContext(DbContextOptions<LibraryDbContext> options)
            : base(options)
        {
            Database.EnsureCreated();
        }
    }
}
